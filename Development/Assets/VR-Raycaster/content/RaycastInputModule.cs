﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
//using HutongGames.PlayMaker;

public class RaycastInputModule : MonoBehaviour {
    
	RaycastHit hit;
	LineRenderer lineRenderer;
	public float rayCastWidth = 0.01f;
	public GameObject raycastTargetPrefab;
	GameObject raycastTarget;

	public SteamVR_TrackedObject controller;

	void Awake(){

		lineRenderer = GetComponent<LineRenderer> ();

		//Instantiate small sphere > visible raycast target
		if (raycastTargetPrefab)
			raycastTarget = GameObject.Instantiate (raycastTargetPrefab, transform.position, Quaternion.identity) as GameObject;

		//assign Steam Controller
		if (transform.parent.GetComponent<SteamVR_TrackedObject> ())
			controller = transform.parent.GetComponent<SteamVR_TrackedObject> ();
		else
			Debug.Log ("Assign Steam Tracked Object to " + transform.name);
	}

	public bool click = false;

	bool GetButtonDown(){

		if (Input.GetKeyDown (KeyCode.Space))
			return true;

		else if (
			SteamVR.active && 
			SteamVR_Controller.Input ((int)controller.index).GetHairTriggerDown ()) {
			Debug.Log (controller.name + " clicked");
			return true;
		}

		else 
			return false;
	}


	//Get Button Events for Raycast Click 
	bool GetButtonUp(){

		if (Input.GetKeyUp (KeyCode.Space))
			return true;

		//Get Input from current Steam Controller (assigned in Awake))
		else if (
			SteamVR.active && 
			SteamVR_Controller.Input ((int)controller.index).GetHairTriggerUp ()) {
			Debug.Log (controller.name + " clicked");
			return true;
		}

		else 
			return false;
	}

    GameObject lastHover;
    void Update()
    {

        Ray ray = new Ray(transform.position, transform.forward);
        var pointerEvent = new PointerEventData(EventSystem.current);

        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {
            var go = hit.collider.gameObject;
            if(go != lastHover)
            {
                TrySendEvent(lastHover, "Pointer Exit");
                ExecuteEvents.ExecuteHierarchy(lastHover, pointerEvent, ExecuteEvents.pointerExitHandler);
				TrySendEvent(go, "Pointer Enter");
                ExecuteEvents.ExecuteHierarchy(go, pointerEvent, ExecuteEvents.pointerEnterHandler);
                lastHover = go;
            }

			if (GetButtonDown())
            {
				TrySendEvent(go, "Pointer Down");
				ExecuteEvents.ExecuteHierarchy(go, pointerEvent, ExecuteEvents.pointerDownHandler);

                TrySendEvent(go, "Pointer Click");
                ExecuteEvents.ExecuteHierarchy(go, pointerEvent, ExecuteEvents.pointerClickHandler);
            }
        }
        else if(lastHover != null)
        {
			TrySendEvent(lastHover, "Pointer Exit");
            ExecuteEvents.ExecuteHierarchy(lastHover, pointerEvent, ExecuteEvents.pointerExitHandler);
            
			lastHover = null;
        }

		if (GetButtonUp()) {
			if (lastHover != null) {
				TrySendEvent(lastHover, "Pointer Up");
				ExecuteEvents.ExecuteHierarchy(lastHover, pointerEvent, ExecuteEvents.pointerUpHandler);
			}
		}

		DrawLineRenderer ();
    }

	//Send Events to Playmaker
    void TrySendEvent(GameObject go, string eventName)
    {
        if (go == null) return;
        var fsm = go.GetComponent<PlayMakerFSM>();
        if (fsm != null)
            fsm.SendEvent(eventName);
    }

	//Read current position of raycast target (f.e. for playmaker)
	public Vector3 GetRaycastTargetPosition(){

		Vector3 pos = new Vector3();

		if(hit.transform != null){
			pos = hit.point;
		}
		else
			pos = Vector3.zero;

		return pos;
	}


	//Only visible in Editor! Debugging
	void OnDrawGizmos(){
		Gizmos.DrawLine (transform.position, hit.point);
	}

	//Draws LineRenderer as Raycast
	void DrawLineRenderer(){
		lineRenderer.SetPosition (0, transform.position);
		lineRenderer.SetPosition (1, hit.point);

		lineRenderer.SetWidth (rayCastWidth, rayCastWidth/2);

		if (raycastTarget)
			raycastTarget.transform.position = hit.point;
	}

	//raycastTarget not child of controller (is enabled/disabled, while visible(invisible)
	void OnEnable(){

		if (raycastTarget)
			raycastTarget.SetActive (true);
	}

	void OnDisable(){
	
		if (raycastTarget)
			raycastTarget.SetActive (false);
	}


}
